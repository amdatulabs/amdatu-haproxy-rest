package stats


import (
	"net"
	"fmt"
	"bufio"
	"errors"
	"strings"
	"encoding/json"
	"encoding/csv"
	"io"
	"bytes"
	"bitbucket.org/amdatulabs/amdatu-haproxy-rest/structs"
	"github.com/bitly/go-simplejson"
)


var Socket string

func SetSocket(socket string) {
	Socket = socket
}

// Get HAProxy stats from it's configured stats socket.
func GetStatsByStatsType(statsGroup string) ([]structs.StatsGroup, error) {
	var stats []structs.StatsGroup
	var cmdString string

	switch statsGroup {
	case "all":
		cmdString = "show stat -1\n"
	case "backend":
		cmdString = "show stat -1 2 -1\n"
	case "frontend":
		cmdString = "show stat -1 1 -1\n"
	case "server":
		cmdString = "show stat -1 4 -1\n"
	default:
		return nil, errors.New("Invalid stats parameter \nValid paths are: \n\nall \nbackend \nfrontend \nserver \nserver/{servername} \nserver/{servername}/{stattype}")
	}

	result, err := haproxyCmd(cmdString)
	if err != nil {
		return stats, err
	} else {
		result, err := parse_csv(strings.Trim(result,"# "))
		if err != nil {
			return stats, err
		} else {
			err := json.Unmarshal([]byte(result), &stats)
			if err != nil {
				return stats, err
			} else {
				return stats, nil
			}
		}

	}
}

// Get stats for a specific backend.
func GetStatsForBackend(backendName string) (structs.StatsGroup, error) {
	allStats, err := GetStatsByStatsType("backend")
	var stats structs.StatsGroup
	if err != nil {
		return stats,err
	} else {
		for _, stats = range allStats {
			if strings.EqualFold(stats.Pxname, backendName) {
				return stats,nil
			}
		}
		return stats,errors.New("Backend could not be found")
	}
}

// Get one specific stat for a specific backend.
func GetSingleStatForBackend(backendName string, statType string) (*simplejson.Json, error) {
	stat, err := GetStatsForBackend(backendName)
	if err != nil {
		return nil,err
	} else {
		return getStatByJSONTag(stat, statType)
	}
}

// Get stats for a specific server.
func GetStatsForServer(serverName string) (structs.StatsGroup, error) {
	allStats, err := GetStatsByStatsType("server")
	var stats structs.StatsGroup
	if err != nil {
		return stats,err
	} else {
		for _, stats = range allStats {
			if strings.EqualFold(stats.Svname, serverName) {
				return stats,nil
			}
		}
		return stats,errors.New("Server could not be found")
	}
}

// Get one specific stat for a specific server.
func GetSingleStatForServer(serverName string, statType string) (*simplejson.Json, error) {
	stat, err := GetStatsForServer(serverName)
	if err != nil {
		return nil,err
	} else {
		return getStatByJSONTag(stat, statType)
	}
}

// Executes an arbitrary HAproxy command on the unix socket
func haproxyCmd(cmd string) (string, error){
	// Connect to haproxy socket
	conn, err_conn := net.Dial("unix", Socket)

	if err_conn != nil {
		return "", errors.New("Unable to connect to Haproxy socket")
	} else {
		defer conn.Close()

		fmt.Fprint(conn, cmd)
		response := ""
		scanner := bufio.NewScanner(conn)
		for scanner.Scan() {
			response += (scanner.Text() + "\n")
		}
		if err := scanner.Err(); err != nil {
			return "", err
		} else {
			return response, nil
		}
	}
}

// Retrieves the value for a specific field using it's JSON tag.
func getStatByJSONTag(stats structs.StatsGroup, statType string) (*simplejson.Json, error) {
	json, err := json.Marshal(stats)
	js, err := simplejson.NewJson(json)
	if err != nil {
		return nil, err
	}
	if statType != "" {
		val := js.Get(statType)
		if val.MustString() !=  "" {
			return val, nil
		}
	}
	return nil, errors.New("Invalid statType parameter")
}

// Parses the raw stats CSV output to a JSON string
func parse_csv(csvInput string) (string, error){
	csvReader := csv.NewReader(strings.NewReader(csvInput))
	lineCount := 0
	var headers []string
	var result bytes.Buffer
	var item bytes.Buffer
	result.WriteString("[")

	for {
		record, err := csvReader.Read()
		if err == io.EOF {
			result.Truncate(int(len(result.String())-1))
			result.WriteString("]")
			break
		} else if err != nil {
			fmt.Println("Error:", err)
			return "", err
		}
		if lineCount == 0 {
			headers = record[:]
			lineCount += 1
		} else {
			item.WriteString("{")
			for i := 0; i < len(headers); i++ {
				item.WriteString("\"" + headers[i] + "\": \"" + record[i] + "\"")
				if i == (len(headers)-1) {
					item.WriteString("}")
				} else {
					item.WriteString(",")
				}
			}
			result.WriteString(item.String() + ",")
			item.Reset()
			lineCount += 1
		}
	}
	return result.String(), nil
}