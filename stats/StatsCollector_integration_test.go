// +build integration

package stats

import (
	"testing"
	"strings"
)

var SocketLocation = "/tmp/socket"

func init() {
	SetSocket(SocketLocation)
}

func TestSetSocketToUnexistingSocketShouldntBeAbleToConnect(t *testing.T) {
	expected := "Unable to connect to Haproxy socket"
	SetSocket("/tmp/invalidsocket")
	_, err := GetStatsByStatsType("all")
	if (err != nil) {
		actual := err.Error()
		if (!strings.EqualFold(expected, actual)) {
			t.Errorf("Test failed, expected: '%s', got:  '%s'", expected, actual)
		}
	} else {
		t.Errorf("Test failed, expected '%s', got stats results instead", expected)
	}
	SetSocket(SocketLocation)
}

func TestGetAllStatsCount(t *testing.T) {
	expected := 8
	stat, err := GetStatsByStatsType("all")
	if (err != nil) {
		t.Errorf("Test failed, expected: '%d' StatsGroup server results, got:  '%d'", expected, err)
	}
	if (stat != nil) {
		actual := len(stat)
		if (actual != expected) {
			t.Errorf("Test failed, expected: '%d' StatsGroup results, got:  '%d'", expected, actual)
		}
	}
}

func TestGetServerStatsCount(t *testing.T) {
	expected := 3
	stat, err := GetStatsByStatsType("server")
	if (err != nil) {
		t.Errorf("Test failed, expected: '%d' StatsGroup server results, got:  '%d'", expected, err)
	}
	if (stat != nil) {
		actual := len(stat)
		if (actual != expected) {
			t.Errorf("Test failed, expected: '%d' StatsGroup server results, got:  '%d'", expected, actual)
		}
	}
}

func TestGetSingleStatForServerThatShouldBeDOWN(t *testing.T) {
	expected := "DOWN"
	stat, err := GetSingleStatForServer("offline_server_1", "status")
	if (err != nil) {
		t.Error(err)
	}
	actual := stat.MustString()
	if (!strings.EqualFold(expected, actual)) {
		t.Errorf("Test failed, expected: '%s', got:  '%s'", expected, actual)
	}
}

func TestGetSingleStatForServerThatShouldBeUP(t *testing.T) {
	expected := "UP"
	stat, err := GetSingleStatForServer("online_server", "status")
	if (err != nil) {
		t.Error(err)
	}
	actual := stat.MustString()
	if (!strings.EqualFold(expected, actual)) {
		t.Errorf("Test failed, expected: '%s', got:  '%s'", expected, actual)
	}
}

func TestGetNonExistingStatGroupShouldLeadToInvalidStatsParam(t *testing.T) {
	expected := "Invalid stats parameter \nValid paths are: \n\nall \nbackend \nfrontend \nserver \nserver/{servername} \nserver/{servername}/{stattype}"
	_, err := GetStatsByStatsType("invalidparam")
	if (err != nil) {
		actual := err.Error()
		if (!strings.EqualFold(expected, actual)) {
			t.Errorf("Test failed, expected: '%s', got:  '%s'", expected, actual)
		}
	} else {
		t.Errorf("Test failed, expected '%s', got stats results instead", expected)
	}
}

func TestGetStatsForNonExistingServerShouldLeadToServerNotFound(t *testing.T) {
	expected := "Server could not be found"
	_, err := GetSingleStatForServer("non_existing_server", "status")
	if (err != nil) {
		actual := err.Error()
		if (!strings.EqualFold(expected, actual)) {
			t.Errorf("Test failed, expected: '%s', got:  '%s'", expected, actual)
		}
	} else {
		t.Errorf("Test failed, expected '%s', got stats results instead", expected)
	}
}

func TestGetNonExistingStatForServerShouldLeadToInvalidStatTypeParam(t *testing.T) {
	expected := "Invalid statType parameter"
	_, err := GetSingleStatForServer("offline_server_1", "invalid_stat")
	if (err != nil) {
		actual := err.Error()
		if (!strings.EqualFold(expected, actual)) {
			t.Errorf("Test failed, expected: '%s', got:  '%s'", expected, actual)
		}
	} else {
		t.Errorf("Test failed, expected '%s', got stats results instead", expected)
	}
}