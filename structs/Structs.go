package structs

// Struct to hold the output from the /stats endpoint
type StatsGroup struct {
	Pxname string `json:"pxname"`
	Svname string `json:"svname"`
	Qcur string `json:"qcur"`
	Qmax string `json:"qmax"`
	Scur string `json:"scur"`
	Smax string `json:"smax"`
	Slim string `json:"slim"`
	Stot string `json:"stot"`
	Bin string `json:"bin"`
	Bout string `json:"bout"`
	Dreq string `json:"dreq"`
	Dresp string `json:"dresp"`
	Ereq string `json:"ereq"`
	Econ string `json:"econ"`
	Eresp string `json:"eresp"`
	Wretr string `json:"wretr"`
	Wredis string `json:"wredis"`
	Status string `json:"status"`
	Weight string `json:"weight"`
	Act string `json:"act"`
	Bck string `json:"bck"`
	Chkfail string `json:"chkfail"`
	Chkdown string `json:"chkdown"`
	Lastchg string `json:"lastchg"`
	Downtime string `json:"downtime"`
	Qlimit string `json:"qlimit"`
	Pid string `json:"pid"`
	Iid string `json:"iid"`
	Sid string `json:"sid"`
	Throttle string `json:"throttle"`
	Lbtot string `json:"lbtot"`
	Tracked string `json:"tracked"`
	_Type string `json:"type"`
	Rate string `json:"rate"`
	Rate_lim string `json:"rate_lim"`
	Rate_max string `json:"rate_max"`
	Check_status string `json:"check_status"`
	Check_code string `json:"check_code"`
	Check_duration string `json:"check_duration"`
	Hrsp_1xx string `json:"hrsp_1xx"`
	Hrsp_2xx string `json:"hrsp_2xx"`
	Hrsp_3xx string `json:"hrsp_3xx"`
	Hrsp_4xx string `json:"hrsp_4xx"`
	Hrsp_5xx string `json:"hrsp_5xx"`
	Hrsp_other string `json:"hrsp_other"`
	Hanafail string `json:"hanafail"`
	Req_rate string `json:"req_rate"`
	Req_rate_max string `json:"req_rate_max"`
	Req_tot string `json:"req_tot"`
	Cli_abrt string `json:"cli_abrt"`
	Srv_abrt string `json:"srv_abrt"`
	Comp_in string `json:"comp_in"`
	Comp_out string `json:"comp_out"`
	Comp_byp string `json:"comp_byp"`
	Comp_rsp string `json:"comp_rsp"`
	Lastsess string `json:"lastsess"`
	Last_chk string `json:"last_chk"`
	Last_agt string `json:"last_agt"`
	Qtime string `json:"qtime"`
	Ctime string `json:"ctime"`
	Rtime string `json:"rtime"`
	Ttime string `json:"ttime"`
}