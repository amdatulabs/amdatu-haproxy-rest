# DEPRECATED

Since we switched from our custom HAProxy based setup to Kubernetes Ingresses and the Nginx Ingress Controller this project is not maintained anymore




Introduction
===
Amdatu HAProxy REST is a component which can be used to retrieve stats information from a running HAProxy instance.
It is used in the Amdatu HAProxy confd project to provide information about the HAProxy instance running alongside it. It gathers this information from the `stats socket` configured in HAProxy.

A typical use case for this project can be found in the Amdatu Kubernetes Deployer, which polls the REST endpoint for a server's status to determine whether a deployment is correctly registered in HAProxy or not.

Related components
===
This project is tightly coupled to Amdatu HAProxy confd, the haproxy-rest binary is added to the Docker image created by the HAProxy confd Dockerfile. When the HAProxy confd container is started, haproxy-rest is also started.

* [Amdatu HAProxy confd](https://bitbucket.org/amdatulabs/amdatu-haproxy-confd) is a configuration template to use HAProxy together with Kubernetes and the Amdatu Kubernetes Deployer.

Building
===
Amdatu HAProxy REST is a Go project, assuming the source is checked out into a [properly configured Go environment's](https://golang.org/doc/code.html#Workspaces) source directory, it can be built by running `go install` from the project's root. A binary called `haproxy-rest` should then appear in the bin folder of the Go path.

Running tests
===
* Currently this project only provides integration tests.

To run this project's tests, a webserver available on port 8080 and an HAProxy instance need to run on the side, using the provided configuration (see `test/haproxy.cfg`).
This setup should provide a reachable backend with an in HAProxy configured server (bound to the available webserver) on port 8080, and an unreachable backend with two configured servers on port 8181. How to set this up and run the tests is described in the following steps:

### Setting up a simple webserver using Jetty
For the webserver, [Jetty](http://download.eclipse.org/jetty/)'s demo-base can be easily used for example;

1. Download the archive (9.3.7.v20160115 at the time of writing this)
2. Extract the archive
3. Change the directory to demo-base (run `cd path_to_archive_contents/demo-base/`)
4. Run `java -jar ../start.jar`

### Running HAProxy
The integration tests rely on a running HAProxy instance started with the provided haproxy.cfg. The easiest way to accomplish this is by installig it through your favorite package manager. The following steps describe how to do this on OSX (assuming [Homebrew](http://brew.sh/) is installed):

1. Run `brew update`
2. Run `brew install haproxy`
3. Start HAProxy using the provided HAProxy test configuration (run `haproxy -f /path_to_golang_src/haproxy-rest/test/haproxy.cfg`)

### Running the integration tests
To run the tests after setting everything up, run `go test ./... -tags=integration` from the root source folder.

Getting started
===

To be able to use Amdatu HAProxy REST, a small requirement in HAProxy's configuration has to be met, a stats socket has to be configured:

```
global
    daemon
    stats socket /tmp/socket
```
 
Running Amdatu HAProxy REST as a standalone application alongside an HAProxy instance:

```
haproxy-rest -p=8787 -s="/tmp/haproxysocket"
```

There are two configuration parameters, -p to tell haproxy-rest on which port to run, and -s to tell where the HAProxy instance's [socket](https://cbonte.github.io/haproxy-dconv/configuration-1.6.html#3.1-stats%20socket) can be found.

Example Usage
===

Amdatu HAProxy REST provides all stats on the front- and backends and individual servers configured, just like they can be found on HAProxy's stats page. It is possible to request all stats at once, all stats per category (frontend, backend, server), per single instance of each category, or a single stat of a single instance.

* Requesting all stats at once:

```
http://127.0.0.1:8787/stats/all
```

* Requesting all for a single category {backend, server, frontend}, all backends in this case:

```
http://127.0.0.1:8787/stats/backend
```

* Requesting all for a single instance of a category {backend, server, frontend}:

```
http://127.0.0.1:8787/stats/server/my_server_1
```

* Requesting a single stat for a single instance of a category {backend, server, frontend}:

```
http://127.0.0.1:8787/stats/server/my_server_1/status
```


Getting involved
===

[TODO-LINK-JIRA](https://amdatu.atlassian.net/projects/AKD) and of course pull requests are greatly appreciated!
The project is built on [TODO-LINK-BAMBOO](https://amdatu.atlassian.net/builds/browse/AKD-MAIN/latest).