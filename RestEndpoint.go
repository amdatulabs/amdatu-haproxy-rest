package main

import (
	"net/http"
	"log"
	"fmt"
	"encoding/json"
	"flag"
	"bitbucket.org/amdatulabs/amdatu-haproxy-rest/stats"
	"github.com/gorilla/mux"
)

func main() {
	port := flag.Int("p", 8787, "Port number to listen on")
	socket := flag.String("s", "/tmp/socket", "Stats socket as configured in HAProxy")
	flag.Parse()

	// Set correct socket for stats collector to use
	stats.SetSocket(*socket)

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/stats/{statsType}", getStats)
	router.HandleFunc("/stats/backend/{backendName}", getBackendStats)
	router.HandleFunc("/stats/backend/{backendName}/{statType}", getBackendStat)
	router.HandleFunc("/stats/server/{serverName}", getServerStats)
	router.HandleFunc("/stats/server/{serverName}/{statType}", getServerStat)

	// Set port to listen on
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d",*port), router))
}

// Get all stats for a specific HAProxy group
func getStats(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	statsType := vars["statsType"]
	stats, err := stats.GetStatsByStatsType(statsType)
	if err != nil {
		fmt.Fprintf(writer, err.Error())
	} else {
		json.NewEncoder(writer).Encode(stats)
	}
}

// Get stats for a specific backend
func getBackendStats(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	backendName := vars["backendName"]
	stats, err := stats.GetStatsForBackend(backendName)
	if err != nil {
		http.Error(writer, "Backend could not be found", 404)
	} else {
		json.NewEncoder(writer).Encode(stats)
	}
}

// Get one specific stat for a specific backend
func getBackendStat(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	backendName := vars["backendName"]
	statType := vars["statType"]
	stat, err := stats.GetSingleStatForBackend(backendName, statType)
	if err != nil {
		fmt.Fprintf(writer, err.Error())
	} else {
		json.NewEncoder(writer).Encode(stat)
	}

}

// Get stats for a specific server
func getServerStats(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	serverName := vars["serverName"]
	stats, err := stats.GetStatsForServer(serverName)
	if err != nil {
		http.Error(writer, "Server could not be found", 404)
	} else {
		json.NewEncoder(writer).Encode(stats)
	}
}

// Get one specific stat for a specific server
func getServerStat(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	serverName := vars["serverName"]
	statType := vars["statType"]
	stat, err := stats.GetSingleStatForServer(serverName, statType)
	if err != nil {
		fmt.Fprintf(writer, err.Error())
	} else {
		json.NewEncoder(writer).Encode(stat)
	}

}